<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Tag;

/**
 * @Route("/tag")
 */
class TagController extends Controller {
	
	/**
	 * @Route("/", name="displayTags")
	 * @Method("GET")
	 */
	public function displayTagsAction(Request $request) {
		$tags = array();
		
		foreach($this->getDoctrine()->getRepository('AppBundle:Tag')->findAll() as $tag) {
			if(!$this->getUser()) {
				/* S'il s'agit d'un visiteur anonyme et qu'il y a au moins un commentaire public portant le tag, alors ajout dans l'array */ 
				if(count($this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByTag($tag, 0)) > 0) {
					array_push($tags, $tag);
				}
			} else {
				foreach($this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByTag($tag, 2) as $comment) {
					/* S'il s'agit d'un utilisateur connecté et que le tag figure sur un de ses propres commentaires, un commentaire public ou un commentaire réservé aux amis et qu'on dispose de la visibilité, alors ajout dans l'array */
					if(($this->visibleTags($comment))) {
						array_push($tags, $tag);
					}
				}
			}
		}
		
		return $this->render('tag/displayTags.html.twig', array('tags' => $tags));
	}
	
	/**
	 * @Route("/{tagname}/", name="displayTagComments")
	 * @Method("GET")
	 */
	public function displayTagCommentsAction($tagname, Request $request) {
		$currentTag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneByName($tagname); // Récupération du tag
		$comments = array();
		
		/* Vérification que le tag existe */
		if($currentTag === null) {
			$currentTag = new Tag();
			$currentTag->setName($tagname); // Si oui, on crée un objet tag avec le nom (sans ajout à la base) pour la vue
		} else {
			/* Vérification des commentaires contenant le tag */
			foreach($this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByTag($currentTag, 2) as $comment) {
				if($this->visibleTags($comment)) {
					array_push($comments, $comment);
				}
			}
		}

		return $this->render('tag/displayTagComments.html.twig', array('tag' => $currentTag, 'comments' => $comments));
	}
	
	/**
	 * @Route("/{tagname}/stats/", name="displayTagStats")
	 * @Method("GET")
	 */
	public function displayTagStatsAction($tagname, Request $request) { 
		$currentTag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneByName($tagname); // Récupération du tag
		$comments = array();
		
		/* Vérification que le tag existe */
		if($currentTag === null) {
			$currentTag = new Tag();
			$currentTag->setName($tagname); // Si oui, on crée un objet tag avec le nom (sans ajout à la base) pour la vue
		} else {
			/* Vérification des commentaires contenant le tag */
			foreach($this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByTag($currentTag, 2) as $comment) {
				if($this->visibleTags($comment)) {
					array_push($comments, $comment);
				}
			}
		}
		
		return $this->render('tag/displayTagStats.html.twig', array('tag' => $currentTag, 'comments' => $comments));
	}
	
	/**
	 * @Route("/{tagname}/{date}/", name="searchTagDate", requirements={
	 *     "date": "[0-9]{2}-[0-9]{2}-[0-9]{4}"
	 * })
	 * @Method("GET")
	 */
	public function searchTagDateAction($tagname, $date, Request $request) {
		$currentTag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneByName($tagname); // Récupération du tag
		$date = new \DateTime($date);
		$comments = array();
		
		/* Vérification que le tag existe */
		if($currentTag === null) {
			$currentTag = new Tag();
			$currentTag->setName($tagname);
		} else {
			/* Vérification des commentaires contenant le tag à la date donnée */
			foreach($this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByDateAndTag($date, $currentTag, 2) as $comment) {
				if($this->visibleTags($comment)) {
					array_push($comments, $comment);
				}
			}
		
		}
		return $this->render('tag/searchTagDate.html.twig', array(
			'tag' => $currentTag,
			'date' => $date,
			'comments' => $comments
		));
	}
	
	private function visibleTags($comment) {
		$repoUser = $this->getDoctrine()->getRepository('AppBundle:User');
		return (($comment->getPrivacy() == 0) || ($comment->getPrivacy() == 1 && in_array($comment->getUser(), $repoUser->findOneContacted($this->getUser()))) || ($comment->getUser() === $this->getUser()));
	}
}