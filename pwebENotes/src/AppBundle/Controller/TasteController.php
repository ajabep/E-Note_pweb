<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Taste;

/**
 * @Route("/taste")
 */
class TasteController extends Controller {
	
	/**
	 * @Route("/add/{idComment}/", name="addTaste")
	 * @Method("POST")
	 */
	public function addTasteAction($idComment, Request $request) {
		$taste2add = new Taste();
		
		/* Récupération du commentaire */
		$comment = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findOneById($idComment);
		
		/* Vérification du commentaire */
		if($comment === null) {
			$request->getSession()->getFlashBag()->add('danger', 'Le commentaire sur lequel vous essayer d\'ajouter un avis n\'existe pas.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}

		$taste = (int) $request->request->get('taste');

		/* Récupération de l'utilisateur qui like/dislike */
		if($this->getUser()) {
			$taste2add->setUser($this->getUser());
		} else {
			$request->getSession()->getFlashBag()->add('danger', 'Veuillez vous authentifier avant d\'ajouter votre avis sur un commentaire.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayComment', array('username' => $comment->getUser()->getUsername(), 'idComment' => $comment->getId()));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}

		/* Récupération du taste */
		$taste = (int) $request->request->get('taste');

		/* Vérification de l'utilisateur */
		if($this->getUser() == $comment->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas aimer ce commentaire. En effet, vous en êtes l\'auteur.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayComment', array('username' => $comment->getUser()->getUsername(), 'idComment' => $comment->getId()));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification du taste */
		if(!($taste == 0 || $taste == 1)) {
			$request->getSession()->getFlashBag()->add('danger', 'Une erreur est survenue lors de la reconnaissance de l\'avis. Veuillez ré-essayer. Si cette opération ne marche toujours pas, veuillez contacter l\'administration du site. Merci.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayComment', array('username' => $comment->getUser()->getUsername(), 'idComment' => $comment->getId()));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Définition des valeurs dans l'objet */
		$taste2add->setCommentaire($comment);
		$taste2add->setTaste($taste);
		
		/* Récupération d'un potentiel vote déjà existant */
		$existingTaste = new Taste();
		$existingTaste = $this->getDoctrine()->getRepository('AppBundle:Taste')->findOneByUserAndComment($taste2add->getUser(), $taste2add->getCommentaire());
		$em = $this->getDoctrine()->getManager();

		if($existingTaste === null || count($existingTaste) == 0) {
			$em->persist($taste2add); // Ajout du vote demandé si le commentaire n'a pas encore été voté par l'user
		} else if($existingTaste[0]->getTaste() == $taste) {
			$em->remove($existingTaste[0]); // Remove du vote existant si le vote demandé est identique
		} else {
			$existingTaste[0]->setTaste(($existingTaste[0]->getTaste() == 1)? 0:1);
			$em->persist($existingTaste[0]); // Changement du vote existant si celui-ci est différent du vote demandé
		}

		$em->flush();
		$request->getSession()->getFlashBag()->add('success', 'Votre vote a bien été pris en compte.');
		
		/* Envoi du visiteur vers son ancienne URL ou, à défaut, sur la page du commentaire */
		$httpReferer = $request->headers->get('REFERER');
		
		if (empty($httpReferer)) {
			$httpReferer = $this->generateUrl('displayComment', array('username' => $comment->getUser()->getUsername(), 'idComment' => $comment->getId()));
		}
		
		return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
	}
}