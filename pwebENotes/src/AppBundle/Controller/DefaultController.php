<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller {
	
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request) {
		$user = $this->getUser();

		if ($user != null)
			return $this->forward('AppBundle:User:home');
		
		return $this->render('default/index.html.twig');
	}
	
	/**
	 * Pour la compatibilité 
	 * Redirige vers le profil
	 * 
	 * @Route("/profile/", name="fos_user_profile_show")
	 * @Method("GET")
	 */
	public function fosUserProfileShowAction(Request $request) {
		$user= $this->getUser();
		
		if ($user == null)
			throw new NotFoundHttpException('L\'utilisateur n\'est pas connecté.');

		return $this->redirectToRoute('displayProfile', array('username' => $user->getUsername()), 308);
	}
}