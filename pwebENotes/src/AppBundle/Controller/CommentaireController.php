<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Commentaire;
use AppBundle\Entity\Tag;
use AppBundle\Form\CommentaireEditType;

/**
 * @Route("/commentaire")
 */
class CommentaireController extends Controller {
	
	/**
	 * @Route("/add/", name="addComment")
	 * @Method("POST")
	 */
	public function addCommentAction(Request $request) {
		$comment = new Commentaire();
		
		/* Récupération de l'auteur du commentaire */
		if($this->getUser()) {
			$comment->setUser($this->getUser());
		} else {
			$request->getSession()->getFlashBag()->add('danger', 'Veuillez vous authentifier avant de commenter.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Récupération du texte, de la privacy et des noms des tags */
		$text = $request->request->get('text');
		$privacy = $request->request->get('privacy');
		$tagsNames = $request->request->get('tags');
		
		/* Vérification de la présence d'un texte */
		$textToCheck = trim($text);
		if(strlen($textToCheck) === 0) {
			$request->getSession()->getFlashBag()->add('danger', 'Veuillez entrer un texte à commenter.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		                                    
		/* Vérification de la visibilité */
		if($privacy < 0 || $privacy > 2) {
			$request->getSession()->getFlashBag()->add('danger', 'Veuillez entrer une visibilité valide.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Définition des valeurs dans l'objet */
		$comment->setText($text);
		$comment->setPrivacy($privacy);
		
		/* Vérification de la syntaxe des tag et ajout si correct */
		$tagsNames = explode(",", $tagsNames);
		foreach($tagsNames as $tag) {
			$tag = trim($tag);
			if(strlen($tag) == 0) {
				continue;
			}
			if(strpos($tag, '#') === 0)  {
				$tag = substr($tag, 1);
			}
			if(!preg_match('/[a-zA-Z0-9]+/', $tag)) {
				$request->getSession()->getFlashBag()->add('danger', 'Veuillez entrer des tags valides. Les tags valides sont uniquement constitués de caractères alphanumériques.');
				
				$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
				
				if (empty($httpReferer)) {
					$httpReferer = $this->generateUrl('homepage');
				}
				
				return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
			}
			$existingTag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneBy(array('name' => $tag));
			if($existingTag !== null) {
				$comment->addTag($existingTag);
			} else {
				$tag2add = new Tag();
				$tag2add->setName($tag);
				$comment->addTag($tag2add);
			}
		}
		
		/* Introduction de l'objet dans la base */
		$em = $this->getDoctrine()->getManager();
		$em->persist($comment);
		$em->flush();
		$request->getSession()->getFlashBag()->add('success', 'Votre commentaire a bien posté');
		
		return $this->redirect($this->generateUrl('displayComment', array('username' => $this->getUser()->getUsername(), 'idComment' => $comment->getId())));
	}
	
	/**
	 * @Route("/{idComment}/modifier/", name="editComment")
	 * @Method({"POST", "GET"})
	 */
	public function editCommentAction($idComment, Request $request) {
		
		/* Vérification que l'user est connecté */
		if(!$this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Veuillez vous authentifier avant de modifier votre commentaire.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
	
		/* Récupération du commentaire */
		$comment = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findOneById($idComment);
		
		/* Vérification que le commentaire existe */
		if($comment === null) {
			$request->getSession()->getFlashBag()->add('danger', 'Le commentaire que vous souhaitez éditer n\'existe pas.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification que l'user est bien l'auteur du commentaire */
		if($comment->getUser() !== $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas éditer ce commentaire. En effet, vous n\'en êtes pas l\'auteur.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayComment', array('username' => $comment->getUser()->getUsername(), 'idComment' => $comment->getId()));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Création du formulaire */
		$form = $this->createForm(new CommentaireEditType(), $comment);
		
		/* Si on soumet le form, on réécrit la valeur des tags, qui ne doit pas être une str */
		if ($request->isMethod('POST')){
			$post = &$request->request;
			$tags = array();
			$formSubmition = $post->get('appbundle_commentaireedit');
			$tagsNames = $formSubmition['tags'];
			
			/* Vérification de la syntaxe des tags et ajout si correct */
			$tagsNames = explode(",", $tagsNames);
			foreach($tagsNames as $tag) {
				$tag = trim($tag);
				if(strlen($tag) == 0) {
					continue;
				}
				if(strpos($tag, '#') === 0)  {
					$tag = substr($tag, 1);
				}
				if(!preg_match('/[a-zA-Z0-9]+/', $tag)) {
					$request->getSession()->getFlashBag()->add('danger', 'Veuillez entrer des tags valides. Les tags valides sont uniquement constitués de caractères alphanumériques.');
					
					$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
					
					if (empty($httpReferer)) {
						$httpReferer = $this->generateUrl('homepage');
					}
					
					return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
				}
				$existingTag = $this->getDoctrine()->getRepository('AppBundle:Tag')->findOneBy(array('name' => $tag));
				if($existingTag !== null) {
					$tags[] = $existingTag;
				} else {
					$tag2add = new Tag();
					$tag2add->setName($tag);
					$tags[] = $tag2add;
				}
			}
			
			$formSubmition['tags'] = $tags;
			
			$post->set('appbundle_commentaireedit', $formSubmition);
		}
		
		/* Édition du commentaire */
		if ($form->handleRequest($request)->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($comment);
			$em->flush();
			$request->getSession()->getFlashBag()->add('success', 'Votre commentaire a bien été édité.');
			return $this->redirect($this->generateUrl('displayComment', array('username' => $this->getUser()->getUsername(), 'idComment' => $comment->getId())));
		}

		return $this->render('commentaire/editComment.html.twig', array('comment' => $comment, 'form' => $form->createView()));
	}
	
	/**
	 * @Route("/{idComment}/supprimer/", name="deleteComment")
	 * @Method("GET")
	 */
	public function deleteCommentAction($idComment, Request $request) {
		
		/* Vérification que l'user est connecté */
		if(!$this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Veuillez vous authentifier avant de supprimer un commentaire.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
	
		/* Récupération du commentaire */
		$comment = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findOneById($idComment);
		
		/* Vérification que le commentaire existe */
		if($comment === null) {
			$request->getSession()->getFlashBag()->add('danger', 'Le commentaire que vous souhaitez supprimer n\'existe pas.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification que l'user est bien l'auteur du commentaire */
		if($comment->getUser() !== $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas supprimer ce commentaire. En effet, vous n\'en êtes pas l\'auteur.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayComment', array('username' => $comment->getUser()->getUsername(), 'idComment' => $comment->getId()));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Retrait de l'objet de la base */
		$em = $this->getDoctrine()->getManager();
		$em->remove($comment);
		$em->flush();
		$request->getSession()->getFlashBag()->add('success', 'Votre commentaire bien été supprimé');
		
		return $this->redirect($this->generateUrl('displayProfile', array('username' => $this->getUser()->getUsername())));
	}
	
}