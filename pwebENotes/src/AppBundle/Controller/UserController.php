<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/membre")
 */
class UserController extends Controller {
	
	public function homeAction(Request $request) {
		/* Récupération des commentaires publics de ses contacts et des commentaires réservés aux amis de ses contacts si autorisé */
		$friendsComments = $this->getContactsComments($this->getUser());
		
		return $this->render('user/home.html.twig', array('friendsComments' => $friendsComments));
	}
	
	/**
	 * @Route("/", name="listUsers")
	 * @Method("GET")
	 */
	public function listUsersAction(Request $request) {
		$users = $this->get('fos_user.usermanager')->findUsers(); // Récupération des utilisateurs
  
		return $this->render('user/listUsers.html.twig', array('users' => $users));
	}
	
	/**
	 * @Route("/friends/", name="listFriends")
	 * @Method("GET")
	 */
	public function listFriendsAction(Request $request) {
		/* Vérification que le visiteur est connecté */
		if(!$this->getUser()) {
			throw new NotFoundHttpException('Les cercles d\'amis ne sont disponibles que pour les utilisateurs.');
		}
		
		/* Récupération des amis (= l'utilisateur A a l'utilisateur B en contact et inversement) */
		$friends = array();
		foreach($this->getDoctrine()->getRepository('AppBundle:User')->findAll() as $user) {
			if($this->isContactOf($this->getUser(), $user) && $this->isContactOf($user, $this->getUser())) {
				array_push($friends, $user);
			}
		}
		
		return $this->render('user/listFriends.html.twig', array('friends' => $friends));
	}
	
	/**
	 * @Route("/{username}/", name="displayProfile")
	 * @Method("GET")
	 */
	public function displayProfileAction($username, Request $request) {
		$user2disp = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'utilisateur à afficher

		/* Vérification que l'user existe */
		if($user2disp === null) {
			throw new NotFoundHttpException('L\'utilisateur n\'existe pas !');
		}

		if($user2disp === $this->getUser()) {
			$privacy = 2;
		} elseif($this->getUser() && $this->isContactOf($user2disp, $this->getUser())) {
			$privacy = 1;
		} else {
			$privacy = 0;
		}
		
		$comments2disp = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByUser($user2disp, $privacy); // Récupération des commentaires à afficher

		return $this->render('user/displayProfile.html.twig', array('user' => $user2disp, 'comments' => $comments2disp));
	}
	
	/**
	 * @Route("/{username}/view/{privacy}/", name="displayProfileWithPrivacy", requirements={
	 *     "privacy": "[0-1]"})
	 * @Method("GET")
	 */
	public function displayProfileWithPrivacyAction($username, $privacy, Request $request) {
		$user2disp = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'utilisateur
		
		/* Vérification que l'user ciblé est bien celui connecté */
		if ($user2disp !== $this->getUser()) {
			throw new NotFoundHttpException('Cette opération nécessite d\'être propriétaire du compte.');
		}

		$comments2disp = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByUser($user2disp, $privacy); // Récupération des commentaires
		
		return $this->render('user/displayProfileWithPrivacy.html.twig', array('user' => $user2disp, 'comments' => $comments2disp, 'privacy' => $privacy));
	}
		
	/**
	 * @Route("/{username}/infos/", name="displayInfoProfile")
	 * @Method("GET")
	 */
	public function displayInfoProfileAction($username, Request $request) {
		$user2disp = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'utilisateur

		/* Vérification que l'user existe */
		if($user2disp === null) {
			throw new NotFoundHttpException('L\'utilisateur n\'existe pas !');
		}

		if($user2disp === $this->getUser()) {
			$privacy = 2;
		} elseif($this->getUser() && $this->isContactOf($user2disp, $this->getUser())) {
			$privacy = 1;
		} else {
			$privacy = 0;
		}
		
		$comments2count = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByUser($user2disp, $privacy); // Récupération des commentaires à compter
		
		$tags2disp = $this->getDoctrine()->getRepository('AppBundle:Tag')->findByUser($user2disp, $privacy); // Récupération des tags à afficher
		
		return $this->render('user/displayInfoProfile.html.twig', array('user' => $user2disp, 'commentaires' => $comments2count, 'tags' =>  $tags2disp));
	}
	
	/**
	 * @Route("/{username}/modifier/", name="editProfile")
	 * @Method({"GET", "POST"})
	 */
	public function editProfileAction($username, Request $request) {
		$user2edit = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'user à éditer
		
		/* Vérification que l'user à éditer est bien celui connecté */
		if ($user2edit !== $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas modifier le profil de quelqu\'un d\'autre.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		return $this->forward('FOSUserBundle:Profile:edit');
	}
		
	/**
	 * @Route("/{username}/changer-son-mot-de-passe/", name="changePassword")
	 * @Method({"GET", "POST"})
	 */
	public function changePasswordAction($username, Request $request) {
		$user = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'user dont on veut changer le pass
		
		/* Vérification que l'user dont on veut changer le pass est bien celui connecté */
		if ($user !== $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas modifier le mot-de-passe de quelqu\'un d\'autre.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer))
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		return $this->forward('FOSUserBundle:ChangePassword:changePassword');
	}
	
	/**
	 * @Route("/{username}/contacts/add/", name="addContact")
	 * @Method("GET")
	 */
	public function addContactAction($username, Request $request) {
		$user2add = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'user à ajouter
		
		/* Vérification que l'user à ajouter existe bien */
		if ($user2add === null) {
			$request->getSession()->getFlashBag()->add('danger', 'L\'utilisateur que vous essayez d\'ajouter en contact n\'existe pas.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('homepage');
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification que le visiteur est connecté */
		if(!$this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous devez être connecté pour ajouter un contact');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification que l'utilisateur n'essaie pas de s'ajouter lui-même */
		if ($user2add === $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas vous ajouter en contact.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification que l'utilisateur n'essaie pas de s'ajouter un de ses contacts */
		if ($this->isContactOf($this->getUser(), $user2add)) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous avez déjà ajouté cet utilisateur dans vos contacts.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Ajout de l'user dans les contacts */
		$this->getUser()->addContact($user2add);
		$em = $this->getDoctrine()->getManager();
		$em->persist($this->getUser());
		$em->flush();

		$request->getSession()->getFlashBag()->add('success', 'L\'utilisateur a bien été ajouté dans vos contacts !');

		$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
		
		if (empty($httpReferer)) {
			$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
		}
		
		return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
	}
	
	/**
	 * @Route("/{username}/contacts/remove/", name="rmContact")
	 * @Method("GET")
	 */
	public function rmContactAction($username, Request $request) {
		/* Vérification que le visiteur est connecté */
		if(!$this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous devez être connecté pour supprimer un utilisateur de vos contacts.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		$user2rm = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'user à remove
		
		/* Vérificaion que l'user existe bien */
		if ($user2rm === null) {
			$request->getSession()->getFlashBag()->add('danger', 'L\'utilisateur que vous essayez de supprimer de vos contacts n\'existe pas.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Vérification que l'user n'essaie pas de se suppprimer lui-même */
		if ($user2rm === $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas vous supprimer de vos contacts. Pour supprimer votre compte, contactez l\'administrateur.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}

		if (!$this->isContactOf($this->getUser(), $user2rm)) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas enlever cet utilisateur de vos contacts : il n\'en fait déjà pas parti.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		/* Suppression de l'utilisateur des contacts */
		$this->getUser()->removeContact($user2rm);
		$em = $this->getDoctrine()->getManager();
		$em->persist($this->getUser());
		$em->flush();

		$request->getSession()->getFlashBag()->add('success', 'L\'utilisateur a bien été retiré de vos contacts');

		$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
		
		if (empty($httpReferer))
			$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
		
		return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
	}
	
	/**
	 * @Route("/{username}/contacts/", name="displayContacts")
	 * @Method("GET")
	 */
	public function displayContactsAction($username, Request $request) {
		$user2disp = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'user à afficher
		
		/* Vérification que l'user est bien celui connecté */
		if ($user2disp !== $this->getUser()) {
			$request->getSession()->getFlashBag()->add('danger', 'Vous ne pouvez pas accéder aux contacts d\'un autre utilisateur.');
			
			$httpReferer = $request->headers->get('REFERER'); // Récupération de l'ancienne URL du visiteur
			
			if (empty($httpReferer)) {
				$httpReferer = $this->generateUrl('displayProfile', array('username' => $username));
			}
			
			return $this->redirect($httpReferer); // Redirection vers l'ancienne URL du visiteur
		}
		
		return $this->render('user/displayContacts.html.twig', array(
			'user' => $user2disp
		));
	}
	
	/**
	 * @Route("/{username}/{date}/", name="searchDate", requirements={
	 *     "date": "[0-9]{2}-[0-9]{2}-[0-9]{4}"
	 * })
	 * @Method("GET")
	 */
	public function searchDateAction($username, $date, Request $request) {
		$date = new \DateTime($date);		
		$user2disp = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'utilisateur à afficher
		
		if($user2disp === $this->getUser()) {
			$privacy = 2;
		} elseif($this->getUser() && $this->isContactOf($user2disp, $this->getUser())) {
			$privacy = 1;
		} else {
			$privacy = 0;
		}
		
		$comments2disp = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByDateAndUser($date, $user2disp, $privacy); // Récupération des commentaires à afficher
		
		return $this->render('user/searchDate.html.twig', array(
			'user' => $user2disp,
			'date' => $date,
			'comments' => $comments2disp
		));
	}
	
	/**
	 * @Route("/{username}/{idComment}/", name="displayComment", requirements={
	 *     "idComment": "[0-9]+"
	 * })
	 * @Method("GET")
	 */
	public function displayCommentAction($username, $idComment, Request $request) {
		$user2disp = $this->get('fos_user.usermanager')->findUserByUsername($username); // Récupération de l'user à afficher
		
		$comment = $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findOneById($idComment); // Récupération du comment à afficher
		
		/* Vérification que le commentaire existe */
		if ($comment == null) {
			throw new NotFoundHttpException('Le commentaire n°' . $idComment . ' n\'existe pas.');
		}
		
		/* Vérification que le commentaire appartient bien à l'user demandé */
		if ($comment->getUser() !== $user2disp) {
			throw new NotFoundHttpException('Le commentaire n°' . $idComment . ' n\'appartient à l\'user "' . $username . '".');
		}
		
		/* Vérification que le commentaire est public si l'on est un visiteur */
		if(!$this->getUser() && $comment->getPrivacy() != 0) {
			throw new NotFoundHttpException('Ce commentaire n\'est pas disponible pour les visiteurs. Inscrivez-vous gratuitement !');
		}
		
		/* Vérification que le visiteur est un contact si le commentaire est reservé aux amis */
		if($comment->getPrivacy() == 1 && $comment->getUser() !== $this->getUser() && !$this->isContactOf($user2disp, $this->getUser())) {
			throw new NotFoundHttpException('Ce commentaire n\'est disponible que pour les amis. Vous n\'en êtes pas un.');
		}
		
		/* Vérification que le visiteur est l'auteur du commentaire si celui-ci est privé */
		if($comment->getPrivacy() == 2 && $comment->getUser() !== $this->getUSer()) {
			throw new NotFoundHttpException('Hein hein, euh, quel commentaire ?');
		}
		
		return $this->render('user/displayComment.html.twig', array(
			'user' => $user2disp,
			'comment' => $comment
		));
	}
	
	/**
	 * Le mac sera calculé de la manière suivante
	 * 
	 * mac(user) = SHA1( SHA1( user.id , user.username_canonical , user.email_canonical ) , user.salt )
	 * 
	 * IL NE S'AGIT VOLONTAIREMENT PAS DE HMAC, LAQUELLE EST IMPOSIBLE A FAIRE EN SQL
	 * 
	 * 
	 * @Route("/{mac}/feed.{_format}", name="displayRssFeed", requirements= {
	 * 	"mac" : "[0-9a-f]+",
	 * 	"_format" : "rss"
	 * })
	 * @Method("GET")
	 */
	public function displayFeedComments($mac, Request $request) {
		$users = $this->getDoctrine()->getRepository('AppBundle:User')->findUserByMAC($mac);
		
		if (empty($users[0]))
			throw new NotFoundHttpException('Aucan utilisateur n\'a cette MAC');
		
		$user = $users[0];
		
		$comments = $this->getContactsComments($user);
		$res =  $this->render('user/feed.xml.twig', array('comments' => $comments));
		
		return $res;
	}
	
	private function isContactOf($source, $target) {
		return in_array($source, $this->getDoctrine()->getRepository('AppBundle:User')->findOneContacted($target));
	}
	
	private function getContactsComments($user){
		$friendsComments = array();
		
		/* On parcourt chaque contact */
		foreach($user->getContacts() as $contact) {
			/* Si le contact nous a comme ami aussi, on ajoute les commentaires publics et pour amis, sinon, seulement les publics */
			if($this->isContactOf($contact, $user)) {
				$privacy = 1;
			} else {
				$privacy = 0;
			}
			$friendsComments = array_merge($friendsComments, $this->getDoctrine()->getRepository('AppBundle:Commentaire')->findByUser($contact, $privacy));
		}
		sort($friendsComments); // On trie par date
		
		return $friendsComments;
	}
}