<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SecurityController extends BaseController {

	/**
	 * Juste pour générer une page entière 
	 * 
	 * @Route("/login/", name="loginPage")
	 */
	public function newLoginAction(Request $request) {
		return $this->render('security/login.html.twig');
	}
	
	/**
	 * Overwrite 
	 * 
	 * @param $partOfMenu	if it's a part of the menu
	 */
	public function loginAction(Request $request, $partOfMenu = true) {
		if ($partOfMenu) {
			return $this->newRenderLogin(parent::loginAction($request));
		}
		else {
			return $this->render('security/login_content.html.twig', parent::loginAction($request));
		}
	}

	/**
	 * To make that parent::loginAction return $data
	 */
	protected function renderLogin(array $data) {
		return $data;
	}

	/**
	 * To return the standard parent::renderLogin
	 */
	protected function newRenderLogin(array $data) {
		return parent::renderLogin($data);
	}

	/**
	 * Juste pour générer une page entière et vérifier le honeypot
	 * 
	 * @Route("/register/", name="registerPage")
	 */
	public function registerAction(Request $request) {
		
		// Honey pot
		if (!empty($request->request->get('_destinationdevacances')))
			throw new AccessDeniedHttpException('L\'utilisateur a raté le honeypot');
		
		// Si le honey pot s'est bien passé
		$response = $this->forward('FOSUserBundle:Registration:register');
		return $this->render('security/register.html.twig', array('formContent' => $response->getContent()));
	}
}