<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/message")
 */
class MessageController extends Controller {
	
	/**
	 * Réécrit le controller du FOSMB car rajout de la liste des users ;)
	 * 
	 * Create a new message thread
	 * 
	 * @Route("/new", name="pseudo_fos_message_thread_new")
	 */
	public function newThreadAction()
	{
		$form = $this->container->get('fos_message.new_thread_form.factory')->create();
		$formHandler = $this->container->get('fos_message.new_thread_form.handler');
		
		$usrLst = $this->getDoctrine()->getRepository('AppBundle:User')->findAllBut($this->getUser());

		if ($message = $formHandler->process($form)) {
			return new RedirectResponse($this->container->get('router')->generate('fos_message_thread_view', array(
				'threadId' => $message->getThread()->getId()
			)));
		}

		return $this->container->get('templating')->renderResponse('FOSMessageBundle:Message:newThread.html.twig', array(
			'form' => $form->createView(),
			'data' => $form->getData(),
			'users' => $usrLst
		));
	}
}
