<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\MessageBundle\Model\ParticipantInterface;

/**
 * User
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser implements ParticipantInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var AppBundle\Entity\User
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commentaire", mappedBy="user")
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Taste", mappedBy="user")
     */
    private $tastes;



    /**
     * Get a HMAC for this user
     */
    public function getMac() {
        return sha1(sha1($this->id . $this->username . $this->email) . $this->salt);
    }



    public function __construct() {
        parent::__construct();
    }

    /**
     * Add commentaire
     *
     * @param \AppBundle\Entity\Commentaire $commentaire
     *
     * @return User
     */
    public function addCommentaire(\AppBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    /**
     * Remove commentaire
     *
     * @param \AppBundle\Entity\Commentaire $commentaire
     */
    public function removeCommentaire(\AppBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires->removeElement($commentaire);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Add taste
     *
     * @param \AppBundle\Entity\Taste $taste
     *
     * @return User
     */
    public function addTaste(\AppBundle\Entity\Taste $taste)
    {
        $this->tastes[] = $taste;

        return $this;
    }

    /**
     * Remove taste
     *
     * @param \AppBundle\Entity\Taste $taste
     */
    public function removeTaste(\AppBundle\Entity\Taste $taste)
    {
        $this->tastes->removeElement($taste);
    }

    /**
     * Get tastes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTastes()
    {
        return $this->tastes;
    }

    /**
     * Add contact
     *
     * @param \AppBundle\Entity\User $contact
     *
     * @return User
     */
    public function addContact(\AppBundle\Entity\User $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \AppBundle\Entity\User $contact
     */
    public function removeContact(\AppBundle\Entity\User $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }
}
