<?php

namespace AppBundle\Entity;

/**
 * TasteRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TasteRepository extends \Doctrine\ORM\EntityRepository
{
	public function findOneByUserAndComment(User $user, Commentaire $comment)
    {
        return $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->setParameter('user', $user)
			->andWhere('t.commentaire = :comment')
			->setParameter('comment', $comment)
			->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }
}
