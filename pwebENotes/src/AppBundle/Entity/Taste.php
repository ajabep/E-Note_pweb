<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taste
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TasteRepository")
 */
class Taste
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist","merge","detach","refresh"}, inversedBy="tastes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Commentaire
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Commentaire", cascade={"persist","merge","detach","refresh"}, inversedBy="tastedBy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commentaire;

    /**
     * @var boolean
     *
     * @ORM\Column(name="taste", type="boolean")
     */
    private $taste;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Taste
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set commentaire
     *
     * @param \AppBundle\Entity\Commentaire $commentaire
     *
     * @return Taste
     */
    public function setCommentaire(\AppBundle\Entity\Commentaire $commentaire = null)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return \AppBundle\Entity\Commentaire
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set taste
     *
     * @param boolean $taste
     *
     * @return Taste
     */
    public function setTaste($taste)
    {
        $this->taste = $taste;

        return $this;
    }

    /**
     * Get taste
     *
     * @return boolean
     */
    public function getTaste()
    {
        return $this->taste;
    }
}
