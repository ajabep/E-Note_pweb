#!/bin/bash
# 
# Require root right !
# 
# install nodejs and update npm



# From https://github.com/nodejs/node-v0.x-archive/wiki/Installing-Node.js-via-package-manager#debian-and-ubuntu-based-linux-distributions
echo 'install de nodejs'
curl --silent --location https://deb.nodesource.com/setup_0.12 | bash -
sudo apt-get install --yes nodejs

if [ $(echo `node -v | sed 's#v\(.\)\.#\1#'`'>214.7' | bc) -eq 1 ]
then
	echo 'Oops, your version of nodejs seems me not up-to-date :3'
	echo "I'll wait 20 seconds. As it, you will have the time to read and, if you yant to stop teh process, do ^C ;)"
	sleep 20
fi



# From https://docs.npmjs.com/getting-started/installing-node#updating-npm
echo 'updating npm'
npm update -g npm







