
$(function() {
	
	var container = $('nav').find('form.commentForm').find('.extractorOfFlow');
	
	container.children().each(function() {
		$(this).on('keypress', function(e) {
			if (e.keycode == 27) {
				$('html').focus();
			}
		});
		$(this).on('focus blur', function() {
			container.toggleClass('focus')
		});
	});
	
	$('.commentForm textarea').wysihtml5(
		{
			"font-styles": false,
			"emphasis": true,
			"lists": true,
			"html": false,
			"link": true,
			"image": false,
			"color": false
		}
	);
	setTimeout(function(){focusDispatch();}, 1000);
	
	
	
	$('[data-toggle="tooltip"]').tooltip()
});


function focusDispatch() {
	
	var outOfFlow = $('nav').find('form.commentForm').find('.extractorOfFlow');
	
	var container = $('nav').find('form.commentForm').find('.wysihtml5-sandbox');
	
	container.each(function() {
		var pointer = [this.contentDocument.body || this];
		
		$(pointer).each(function(){$(this).on('keypress', function(e) {
			if (e.keycode == 27) {
				$('html').focus();
			}
		})});
		$(pointer).each(function(){$(this).on('focus blur', function() {
console.log(arguments[0].type, this);
			outOfFlow.toggleClass('focus');
		})});
	});
	
}


