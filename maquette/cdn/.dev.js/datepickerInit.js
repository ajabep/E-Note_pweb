
$(function() {
	$('input[type="date"]').each(function() {
		var a = $(this).parent().find('a')[0];
		var $a = $(a);
		var baseHref = a.href;
		
		if ($(this).val().length != 0)
			baseHref = baseHref.replace(/[0-9]{2}-[0-9]{2}-[0-9]{4}\//, '');
		
		$(this)
			.on('dp.change', function() {
				if ($(this).val().length == 0)
					a.href = baseHref;
				else
					a.href = baseHref +  $(this).val() + '/ ';
			})
			.datetimepicker({
				locale: 'fr',
				showClose: true,
				showClear: true,
				showTodayButton: true,
				toolbarPlacement: 'top',
				format: 'DD-MM-YYYY'
			})
	});
});



