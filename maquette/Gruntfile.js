module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		cdnDir: 'cdn/',
		cssDir: '<%= cdnDir %>css/',
		imgDir: '<%= cdnDir %>img/',
		faviconsDir: '<%= imgDir %>favicons/',
		fontDir: '<%= cdnDir %>fonts/',
		jsDir: '<%= cdnDir %>js/',
		scssDir: '<%= cdnDir %>.scss/',
		imgDevDir: '<%= cdnDir %>.dev.img/',
		faviconsDevDir: '<%= imgDevDir %>favicons/',
		jsDevDir: '<%= cdnDir %>.dev.js/',
		sassCacheDir: '<%= scssDir %>.sass-cache',
		deployDir: '../pwebENotes/web/',
		deployCdnDir: '<%= deployDir %>cdn/',
		
		
		sass: {
			options: {
				  style: 'expanded',
				  trace: true,
				  unixNewlines: true,
				  loadPath: ['<%= scssDir %>'],
				  cacheLocation: '<%= sassCacheDir %>'
			},
			base: {
				files: [{
					expand: true,
					cwd: '<%= scssDir %>',
					src: ['*.scss'],
					dest: '<%= cssDir %>',
					ext: '.css'
				}]
			}
		},
		
		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 8', 'ie 9']
			},
			base: {
				files: [
					{
						expand: true,     // Enable dynamic expansion.
						cwd: '<%= cssDir %>',      // Src matches are relative to this path.
						src: ['**/*.css', '!**/*.min.css'], // Actual pattern(s) to match.
						dest: '<%= cssDir %>',   // Destination path prefix.
					}
				]
			}
		},
		
		cssmin: {
			options: {
				sourceMap: true
			},
			base: {
				files: [
					{
						expand: true,     // Enable dynamic expansion.
						cwd: '<%= cssDir %>',      // Src matches are relative to this path.
						src: ['**/*.css', '!**/*min.css'], // Actual pattern(s) to match.
						dest: '<%= cssDir %>',   // Destination path prefix.
						ext: '.min.css',   // Dest filepaths will have this extension.
						extDot: 'last',   // Extensions in filenames begin after the first dot
					}
				]
			}
		},
		
		imagemin: {
			options: {
			},
			base: {
				files: [
					{
						expand: true,     // Enable dynamic expansion.
						cwd: '<%= imgDevDir %>',      // Src matches are relative to this path.
						src: ['**/*.png', '**/*.jpeg', '**/*.jpg', '**/*.gif', '**/*.svg'], // Actual pattern(s) to match.
						dest: '<%= imgDir %>',   // Destination path prefix.
					},
				],
			}
		},
		
		concat: {
			options: {
				
			},
			base: {
				files: {
					'<%= jsDir %>script.js': ['<%= jsDevDir %>jquery.js', '<%= jsDevDir %>bootstrap.js', '<%= jsDevDir %>wysihtml5-0.3.0.js', '<%= jsDevDir %>bootstrap-wysihtml5.js', '<%= jsDevDir %>general.js'],
				}
			},
			tags: {
				files: {
					'<%= jsDir %>tag.js': ['<%= jsDevDir %>jquery.js', '<%= jsDevDir %>bootstrap.js', '<%= jsDevDir %>wysihtml5-0.3.0.js', '<%= jsDevDir %>bootstrap-wysihtml5.js', '<%= jsDevDir %>moment-with-locale.js', '<%= jsDevDir %>bootstrap-datetimepicker.js', '<%= jsDevDir %>general.js', '<%= jsDevDir %>datepickerInit.js'],
				}
			},
			profile: {
				files: {
					'<%= jsDir %>profile.js': ['<%= jsDevDir %>jquery.js', '<%= jsDevDir %>bootstrap.js', '<%= jsDevDir %>wysihtml5-0.3.0.js', '<%= jsDevDir %>bootstrap-wysihtml5.js', '<%= jsDevDir %>moment-with-locale.js', '<%= jsDevDir %>bootstrap-datetimepicker.js', '<%= jsDevDir %>general.js', '<%= jsDevDir %>datepickerInit.js'],
				}
			},
			comments: {
				files: {
					'<%= jsDir %>comments.js': ['<%= jsDevDir %>jquery.js', '<%= jsDevDir %>bootstrap.js', '<%= jsDevDir %>wysihtml5-0.3.0.js', '<%= jsDevDir %>bootstrap-wysihtml5.js', '<%= jsDevDir %>general.js', '<%= jsDevDir %>comments.js'],
				}
			},
			lists: {
				files: {
					'<%= jsDir %>lists.js': ['<%= jsDevDir %>jquery.js', '<%= jsDevDir %>bootstrap.js', '<%= jsDevDir %>wysihtml5-0.3.0.js', '<%= jsDevDir %>bootstrap-wysihtml5.js', '<%= jsDevDir %>general.js', '<%= jsDevDir %>lists.js'],
				}
			},
		},
		
		uglify: {
			base: {
				// Grunt will search for '**/*.js' under 'lib/' when the 'uglify' task
				// runs and build the appropriate src-dest file mappings then, so you
				// don't need to update the Gruntfile when files are added or removed.
				files: [
					{
						expand: true,     // Enable dynamic expansion.
						cwd: '<%= jsDir %>',      // Src matches are relative to this path.
						src: ['**/*.js', '!**/*min.js'], // Actual pattern(s) to match.
						dest: '<%= jsDir %>',   // Destination path prefix.
						ext: '.min.js',   // Dest filepaths will have this extension.
						extDot: 'last'   // Extensions in filenames begin after the first dot
					},
				],
			},
		},
		
		watch: {
			options: {
				interval: 500 // interval to see a changed file
			},
			sass: {
				files: ['<%= scssDir %>**/*.scss', '<%= scssDir %>**/blocks/**.scss', '!<%= scssCacheDir %>**'],
				tasks: ['clean:css', 'sass', 'autoprefixer']
			},
			stylesheet: {
				files: ['<%= cssDir %>**/*.css', '!<%= cssDir %>**/*.min.css'],
				tasks: ['cssmin']
			},
			images: {
				files: ['<%= imgDevDir %>**/*.png', '<%= imgDevDir %>**/*.jpeg', '<%= imgDevDir %>**/*.jpg', '<%= imgDevDir %>**/*.svg', '<%= imgDevDir %>**/*.svg'],
				tasks: ['clean:img', 'imagemin', 'copy:favicons']
			},
			devScript: {
				files: ['<%= jsDevDir %>**/*.js'],
				tasks: ['clean:js', 'concat']
			},
			script: {
				files: ['<%= jsDir %>**/*.js', '!<%= jsDir %>**/*.min.js'],
				tasks: ['uglify']
			},
			configFiles: {
				files: [ 'Gruntfile.js' ],
				options: {
					reload: true
				}
			}
		},

		clean: {
			cache: {
				src: ['<%= sassCacheDir %>']
			},
			css: {
				src: ['<%= cssDir %>']
			},
			img: {
				src: ['<%= imgDir %>']
			},
			js: {
				src: ['<%= jsDir %>']
			},
			deployCss: {
				src: ['<%= deployDir %><%= cssDir %>']
			},
			deployImg: {
				src: ['<%= deployDir %><%= imgDir %>']
			},
			deployJs: {
				src: ['<%= deployDir %><%= jsDir %>']
			},
		},

		mkdir: {
			cache: {
				options: {
					create: ['<%= sassCacheDir %>']
				},
			},
			css: {
				options: {
					create: ['<%= cssDir %>']
				},
			},
			img: {
				options: {
					create: ['<%= imgDir %>']
				},
			},
			js: {
				options: {
					create: ['<%= jsDir %>']
				},
			},
		},
		
		copy: {
			css: {
				files: [
					{
						expand: true,
						cwd: '<%= cssDir %>',
						src: ['**'],
						dest: '<%= deployDir %><%= cssDir %>',
					},
				],
			},
			font: {
				files: [
					{
						expand: true,
						cwd: '<%= fontDir %>',
						src: ['**'],
						dest: '<%= deployDir %><%= fontDir %>',
					},
				],
			},
			img: {
				files: [
					{
						expand: true,
						cwd: '<%= imgDir %>',
						src: ['**'],
						dest: '<%= deployDir %><%= imgDir %>',
					},
				],
			},
			js: {
				files: [
					{
						expand: true,
						cwd: '<%= jsDir %>',
						src: ['**'],
						dest: '<%= deployDir %><%= jsDir %>',
					},
				],
			},
			favicons: {
				files: [
					{
						expand: true,
						cwd: '<%= faviconsDevDir %>',
						src: ['**.json', '**.xml', '**.ico'],
						dest: '<%= faviconsDir %>',
					},
				],
			},
		},
	});


	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-mkdir');
	grunt.loadNpmTasks('grunt-contrib-copy');


	// Tascks
	var defaultTasks = ['clean:css', 'mkdir:css', 'sass', 'autoprefixer', 'cssmin', 'clean:img', 'imagemin', 'copy:favicons', 'clean:js', 'mkdir:js', 'concat', 'uglify'];
	grunt.registerTask('default', defaultTasks);
	grunt.registerTask('deploy:css', ['clean:css', 'mkdir:css', 'sass', 'autoprefixer', 'cssmin', 'clean:deployCss', 'copy:css']);
	grunt.registerTask('deploy:img', ['clean:img', 'imagemin', 'copy:favicons', 'clean:deployImg', 'copy:img']);
	grunt.registerTask('deploy:js', ['clean:js', 'mkdir:js', 'concat', 'uglify', 'clean:deployJs', 'copy:js']);
	grunt.registerTask('deploy', defaultTasks.concat(['clean:deploy', 'copy']));
	grunt.registerTask('clear:cache', ['clean:cache', 'mkdir:cache']);
	grunt.registerTask('cache:clear', ['clean:cache', 'mkdir:cache']);
	grunt.registerTask('clean:deploy', ['clean:deployCss', 'clean:deployImg', 'clean:deployJs']);

};
