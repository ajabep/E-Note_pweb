# Table de routage

/ -> home
	* présentation
	* inclus un form de login et un form de sign in

/login -> login page
	* login form

/signin -> sign in page
	* sign in form

~~~~~~

## Membres

préfix : /membre

/{userName} -> get user
	* mur du user

/{userName}/info -> get stats du User
	* info du user

/{userName}/{date}/ -> get comments
	* affiche les comments datant du {date} du user {userName}

/{userName}/{id}/ -> get comment
	* display the comment having the id {id}

/{userName}/comme/{liste}/ -> get user
	* vu de ***son profil*** comme une personne de la liste {liste}

/{userName}/modifier/ -> set user
	* modifier ***son profil***

/{userName}/contact/ -> get contacts
	* visualiser  ***ses*** contacts

~~~~~~

## Tags

préfix : /tags

/ -> get tags
	* affiche les tags

/{tag}/ -> get tags
	* affiche les commentaires avec le tag {tag}

/{tag}/{date}/ -> get tags
	* affiche les tags datant du {date} contenant le tag {tag}

